# etg-docker: docker-commands

This is a collection of little scripts I wrote in order to ease the use of docker.

I find the command line options hard to remember, thus I wrote these scripts or extracted the snippets.

Beware: they do what they do without warnings or such.
If a command is deleting data it does so.

So use these commands with care and common sense.


For the complete changelog, see [changelog.md](changelog.md)

## Releases

I don't intend to use a complicated release regime here.

New commands create a new minor release, the release branches are major release branches with according tags.
Let's see how this works out.


## Git-Repository

The branching model regards to the stable mainline model described in <https://www.bitsnbites.eu/a-stable-mainline-branching-model-for-git/>.

This means, there is always a stable mainline, the main branch.
This branch ist always compileable and testable, both without errors.

Features are developed using feature branches.
Special feature branches are used (different from the mainline model) for finalizing releases.

Releases are created as branches of the mainline.
Additionally, each release is tagged, tags contain the patch and, if needed, additional identifiers, such as `rc1` or `beta`.

Patches and minor versions are made in the according release branch.
Major version changes get their own release branch.

## Copyright

Copyright 2021-2022 Ekkart Kleinod <ekleinod@edgesoft.de>

The program is distributed under the terms of the GNU General Public License, either version 3 of the License, or
any later version.

See COPYING for details.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog] and this project adheres to [Semantic Versioning].

## [Unreleased]

### Added

- direct call: `pandoc-docker`



## [1.2.0] - 2024-03-23

### Added

- direct call: `marp-docker`
- direct call: `ocrmypdf-docker`
- direct call: `plantuml-docker`
- script: `list-containers.sh` - list docker containers
- script: `list-images.sh` - list docker images
- script: `load-image.sh` - load docker image from file
- script: `save-image.sh` - save docker image to file from dockerhub


## [1.1.0] - 2022-02-02

### Added

- `rm-dangling-images.sh` - Remove all dangling images

### Changed

- `rm-all-containers.sh` - removed bash line
- `rm-all-images.sh` - removed bash line


## [1.0.0] - 2021-02-28

### Added

- `rm-all-containers.sh` - Remove all stopped containers
- `rm-all-images.sh` - Remove all images


[Unreleased]: https://gitlab.com/etg-docker/docker-commands/-/compare/1.2.0...HEAD
[1.2.0]: https://gitlab.com/etg-docker/docker-commands/-/compare/1.1.0...1.2.0
[1.1.0]: https://gitlab.com/etg-docker/docker-commands/-/compare/1.0.0...1.1.0
[1.0.0]: https://gitlab.com/etg-docker/docker-commands/-/tags/1.0.0

[Keep a Changelog]: https://keepachangelog.com/en/1.1.0/
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html

echo "Load image from file."
echo

filename=$1

# parameter: filename
if [ -z "$filename" ]
then
	echo "no filename given"
	exit 1
fi

tarname=`expr "$filename" : "\(.*\)\.gz"`

gzip --decompress $filename
docker load --input $tarname

# EOF

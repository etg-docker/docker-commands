echo "Remove all dangling images"
echo

docker image rm $(docker image ls --filter "dangling=true" --quiet)

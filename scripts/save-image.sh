echo "Save image to file."
echo

imagename=$1

# parameter: imagename
if [ -z "$imagename" ]
then
	echo "no image name given"
	echo "save-image.sh <imagename>"
	exit 1
fi

filename="$imagename.tar"
filename=`expr "$filename" | sed -r 's/[/]+/_slash_/g'`
filename=`expr "$filename" | sed -r 's/[:]+/_colon_/g'`

docker save --output $filename $imagename
chmod 644 $filename
gzip $filename

# EOF
